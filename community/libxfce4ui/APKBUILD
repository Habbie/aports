# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libxfce4ui
pkgver=4.20.0
pkgrel=0
pkgdesc="Widgets library for the Xfce desktop environment"
url="https://xfce.org/"
arch="all"
license="GPL-2.0-only"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
depends_dev="startup-notification-dev intltool"
makedepends="
	$depends_dev
	gobject-introspection-dev
	gtk+3.0-dev
	gtk-doc
	libepoxy-dev
	libgtop-dev
	libxfce4util-dev
	xfconf-dev
	"
source="https://archive.xfce.org/src/xfce/libxfce4ui/${pkgver%.*}/libxfce4ui-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir=/usr/lib \
		--localstatedir=/var \
		--with-vendor-info="${DISTRO_NAME:-Alpine Linux}" \
		--disable-static \
		--enable-introspection \
		--enable-gtk-doc \
		--enable-glibtop \
		--enable-epoxy
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
33d44e57784542278941803026c9730c7fc680e3bfdebf8656fcd55fcdad3e75846c7aedc940540d8c6c6e88a00c5caec279527dd1db13731c679aa1b9cd7138  libxfce4ui-4.20.0.tar.bz2
"
